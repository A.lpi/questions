﻿# Questions

To contribute to this project, use the Forking Workflow described at <https://gitlab.com/hs-karlsruhe/professional-software-engineering/-/blob/master/Git.md#forking-workflow>.

**List of participants**

-   Stephan Müller
-   Robin Wiezorek
-   Julian Narr
-   Jannik März
-   Dennis Haußmann
-   Alexander Brandstetter
-   Alisa Gänsler
-   Stephan Müller 2
-   Stephan Müller 3
-   Andre Leippi


## Opened questions

## Answered questions
